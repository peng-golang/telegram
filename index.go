package telegram

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

type Message struct {
	ChatID string `json:"chat_id"`
	Text   string `json:"text"`
}

func SendMessage(botToken string, chatID string, text string) {
	msg := Message{ChatID: chatID, Text: text}

	msgStr, _ := json.Marshal(msg)

	// 构建API请求URL
	apiUrl := "https://api.telegram.org/bot" + botToken + "/sendMessage"

	// 发送POST请求以发送消息
	_, err := http.Post(apiUrl, "application/json", bytes.NewBuffer(msgStr))

	if err != nil {
		log.Printf("send telegram message error: %s", err.Error())
	}
}
