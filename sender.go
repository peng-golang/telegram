package telegram

type ITelegramSender interface {
	SendMessage(text string)
}

type Sender struct {
	Token  string
	ChatId string
}

func (s Sender) SendMessage(text string) {
	SendMessage(s.Token, s.ChatId, text)
}
